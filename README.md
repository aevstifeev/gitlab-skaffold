# Gitlab Skaffold

This repository contains [skaffold](https://skaffold.dev/) configuration for a Gitlab development environment. This projects aims to create disposable local `gitlab` instances using Gitlab's production [charts](https://gitlab.com/gitlab-org/charts/gitlab) and docker [images](https://gitlab.com/gitlab-org/build/CNG). This is useful if you need to check various branches, want to quickly test feature you are working on or want to test `k8s` related features.

Skaffold is capable of handling various aspects of the local k8s development: docker builds on file change, k8s deployments, image tag injection and file sync. With local clusters like [k3s](https://k3s.io/), [k3d](https://github.com/rancher/k3d) or [kind](https://kind.sigs.k8s.io/) we can leverage Gitlab's helm charts to run k8s based development environment locally using charts and images that are used in production deployments. Skaffold will inject local images into k8s pods and changed files will be automatically synced into running containers which minimises docker builds.

Please be aware of some [limitations](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/index.md#limitations) of the gitlab's charts (i.e. pages and geo).

# Getting started

To get started with skaffold you will need to setup local k8s cluster. For linux users `k3s` is recommended as it will allow you to use local docker daemon without docker registry. Setup instruction for `k3s` is provided below, other environments might be covered later.

## Using local k3s setup

Install `k3s` locally:

```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v0.9.1 INSTALL_K3S_EXEC="--docker --no-deploy=traefik" sh -
```

`k3s` versions after `0.9.x` are using kubernetes API v1.16 and it's [not supported](https://gitlab.com/gitlab-org/charts/gitlab/issues/1562) by helm chart as of now.

`k3s` uses `containerd` by default, `--docker` argument switches to a `docker` daemon.

`k3s` by default deploys `traefik` ingress controller, we will disable that with `--no-deploy=traefik` and will install `nginx` ingress controller instead.

Setup local storage provider for `k3s`:

```bash
$ mkdir /opt/local-path-provisioner
$ kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml
$ kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

### Configure kubectl

Install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) and setup `k3s` config:

```bash
cp /etc/rancher/k3s/k3s.yaml ~/.kube/config
```

Check that `kubectl` works:

```bash
$ kubectl get nodes
NAME          STATUS   ROLES    AGE   VERSION
ap4y-laptop   Ready    master   8d    v1.15.4-k3s.1
```

### Install helm

Install `helm` v2 CLI by following official [instructions](https://v2.helm.sh/docs/using_helm/#installing-helm). Helm v3 is not [supported](https://gitlab.com/gitlab-org/charts/gitlab/issues/760) by gitlab's charts.

Install [tiller](https://v2.helm.sh/docs/using_helm/#installing-tiller) into the `k3s` cluster or consider using a [tillerless](https://github.com/rimusz/helm-tiller) plugin.

Install `nginx-ingress` chart to the cluster:

```bash
$ helm install stable/nginx-ingress
$ helm list
NAME            REVISION        UPDATED                         STATUS          CHART                   APP VERSION     NAMESPACE
wishing-mastiff 1               Mon Nov 18 10:52:25 2019        DEPLOYED        nginx-ingress-1.24.7    0.26.1          default  
```

Install `metrics-server` chart to the cluster:

```bash
$ helm install stable/metrics-server
```

### Install Kubernetes dashboard (optional)

Apply dashboard manifest and setup admin user in the cluster:

```bash
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
$ kubectl apply -f - <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: default
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: default
EOF
```

Run k8s proxy via `kubectl proxy` and access dashboard at [http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/).

Use token authentication to login, you can get token by running:

```bash
$ kubectl describe secret $(kubectl get secret | grep admin-user | awk '{print $1}')
```

## Using local k3d cluster

[k3d](https://github.com/rancher/k3d) stands for `k3s` in Docker. `k3d` is an alternative option for OSX users. The main difference between `k3s` and `k3d` setups is a necessity for a docker registry: `k3s` cluster will have access to a local Docker daemon which is not the case for a `k3d`. With `k3d` Docker images have to be pushed to a registry and `k3d` cluster will pull them before running.

Install [k3d](https://github.com/rancher/k3d#get) CLI to your local machine and create a new cluster:

```bash
k3d create --api-port 6550 --publish 8081:80 --server-arg '--no-deploy=traefik' --image rancher/k3s:v0.9.1
```

Your new cluster will map port `80` to `8081` and kubernetes API will be available on port `6550`. You can follow [this](https://github.com/rancher/k3d/blob/master/docs/examples.md#connect-with-a-local-insecure-registry) guide to setup local registry and integrate it into `k3d` cluster, alternatively you can use official docker registry.

## Setting up local environment

Install `skaffold` CLI by following official [guide](https://skaffold.dev/docs/install/).

Clone repository and execute `make bootstrap` to clone source repositories and setup base rails image. Update local domain inside `skaffold.yaml`: replace `global.hosts.domain: 192-168-1-119.nip.io` with your cluster's IP address (for `k3s` it will be local IP of the machine).

Run `skaffold dev` to start necessary Docker builds and trigger helm deployment. You can monitor progress through the console or via kubernetes dashboard. Once cluster is up it can be accessed on `gitlab.192-168-1-119.nip.io` (replace with cluster IP). Default username is `root` and password is `5iveL!fe`. Local docker registry is accessible on `registry.192-168-1-119.nip.io`.

Try updating source code under `/gitlab` folder to trigger file sync into your deployed k8s pod's container.

If you are using `k3d` you should re-enable docker push by setting `push: true` in `skaffold.yaml` and prefix all images names with you registry's hostname. 

## Using local cluster for gitlab managed apps and autodevops

You can use your local `k3s` cluster for running gitlab managed apps and autodevops pipelines but you have to be aware of a couple caveats with ingress controller and a local registry.

### Adding local `k3s` cluster to gitlab

Visit `/admin/application_settings/network` and set "Allow requests to the local network from web hooks and services" under "Outbound requests".

Visit `/admin/clusters` and add existing cluster with this parameters:

- API URL: `https://192-168-1-119.nip.io:6443` (replace with your IP). 

- CA Certificate: `kubectl get secret default-token-XXX -o jsonpath="{['data']['ca\.crt']}" | base64 --decode` (replace `default-token-XXX` with value from `kubectl get secrets`)

- Service Token. To get it, execute these 2 commands and get token from the output of the last one:
```bash
$ kubectl apply -f - <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
EOF
$ kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
```

On the next screen set "Base domain" to `192.168.1.118.nip.io` (replace with your IP) and install Helm.

Install `gitlab-runner` for your local gitlab instance. You can do that by installing it as a managed app via cluster management interface or by manually registering local runner.

### Using autodevops locally

Import [waf-enablement-demo](https://gitlab.com/gitlab-org/defend/waf-enablement-demo) to your local gitlab instance.

You need a docker registry enabled on your local gitlab instance, if you are using `skaffold` config then it's already available on `registry.192-168-1-119.nip.io` (replace with your IP). If you are using insecure docker registry (like the one provided by `skaffold` config) then you need to alter CI config to allow Docker In Docker container to work with it. To do that add these lines to the `.gitlab-ci.yml`:

```yaml
build:
  services:
  - name: docker:stable-dind
    command: ["--insecure-registry=registry.192-168-1-119.nip.io"]
```

You also have to enable it for you local docker daemon by creating `/etc/docker/daemon.json` with:
```json
{
  "insecure-registries" : ["registry.192-168-1-119.nip.io"]
}
```

Trigger pipeline for the repository, once it's done your app should be accessible on `http://waf-test-waf-test-demo.192.168.1.118.nip.io` (replace IP and project name).

### Using Gitlab managed ingress

You can install gitlab managed ingress controller into the `gitlab-managed-apps` namespace, you might want to do that if you want to test `ModSecurity` (`Feature.enable(:ingress_modsecurity)`). If you are already running `gitlab` locally through `skaffold` config you most likely already have `nginx` ingress controller in the `default` namespace and installation of another one will fail with port allocation. To resolve that:

- Remove Helm from the `default` namespace:
```bash
$ helm list
NAME            REVISION        UPDATED                         STATUS          CHART                   APP VERSION     NAMESPACE
gitlab          2               Mon Nov 25 12:09:14 2019        DEPLOYED        gitlab-2.4.7            master          default  
quelling-camel  1               Mon Nov 25 11:07:38 2019        DEPLOYED        nginx-ingress-1.25.0    0.26.1          default  
$ helm delete quelling-camel
```

- Install Helm into the `gitlab-managed-apps` using tiller installed into that namespace. To access tiller from the `gitlab-managed-apps` namespace you can use this snippet:

```bash
function gitlab-helm() {
  [ -f ~/.helm/tiller-ca.crt ] || (kubectl get secrets/tiller-secret -n gitlab-managed-apps -o "jsonpath={.data['ca\.crt']}"  | base64 --decode > ~/.helm/tiller-ca.crt)
  [ -f ~/.helm/tiller.crt ]    || (kubectl get secrets/tiller-secret -n gitlab-managed-apps -o "jsonpath={.data['tls\.crt']}" | base64 --decode > ~/.helm/tiller.crt)
  [ -f ~/.helm/tiller.key ]    || (kubectl get secrets/tiller-secret -n gitlab-managed-apps -o "jsonpath={.data['tls\.key']}" | base64 --decode > ~/.helm/tiller.key)
  helm "$@" --tiller-connection-timeout 1 --tls \
    --tls-ca-cert ~/.helm/tiller-ca.crt --tls-cert ~/.helm/tiller.crt \
    --tls-key ~/.helm/tiller.key \
    --tiller-namespace gitlab-managed-apps
}
```

With this snippet you should be able to install ingress controller via:

```bash
$ cd gitlab/vendor/ingress
$ gitlab-helm upgrade ingress stable/nginx-ingress --install --reset-values --version 1.22.1 --set 'rbac.create=true,rbac.enabled=true' --namespace gitlab-managed-apps -f values.yaml --force
```

Once you have enabled ModSecurity feature flag and deployed managed ingress controller you should be able to access you log:

- Go to the `waf-enablement-app` deployment and submit `<script>alert('vulnerable!')</script>`.

- Check modsecurity logs:
```bash
kubectl -n gitlab-managed-apps logs $(kubectl -n gitlab-managed-apps get po | grep ingress-controller | tail -n 1 | awk '{print $1}') modsecurity-log
```

# Useful tips

- You can access rails CLI (`rails`, `bundle` etc.) via `docker exec` or `kubectl-exec` on the `gitlab-unicorn` container. For example:
```bash
$ kubectl exec -ti $(kubectl get po | grep gitlab-unicorn | awk '{print $1}') bash
git@gitlab-unicorn-79c56cc787-447p8:/$ cd /srv/gitlab/
git@gitlab-unicorn-79c56cc787-447p8:/srv/gitlab$ bin/rails c
```

# TODOs

- Figure out a way to do partial docker builds of the monolith into various containers
  
- Add `gitlab-shell`, `gitaly` to the skaffold config

- Consider including `gitlab-runner` chart

- Add ability to persist `postgres` and `redis` PVC

- Clean up PVC for `gitaly`
